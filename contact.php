
<!DOCTYPE HTML>
<html>
<head>
<title>The Free Extro-Electronics Website Template | Contact :: w3layouts</title>
<?php include_once('includes/head.php'); ?>
</head>
<body>
<div class="wrap"> 
	<?php include_once('includes/header.php'); ?>
<?php include_once('includes/header-nav.php'); ?>
<?php include_once('includes/slider.php'); ?>
<div class="content">
	<div class="section group">				
		<div class="col span_1_of_3">
			<div class="contact_info">
			 	<h2>Find Us Here</h2>
				<div class="map">
					<iframe width="100%" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265&amp;output=embed"></iframe><br><small><a href="https://maps.google.co.in/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265" style="color:#666;text-align:left;font-size:12px">View Larger Map</a></small>
				</div>
      		</div>
      		<div class="company_address">
			  	<h2>Company Information :</h2>
			   	<p>Faisalabad,Pakistan</p>
				<p>Zulifqar colony,street # 4,waris pura</p>
				<p>USA</p>
				<p>Phone:(+92) 3007898667</p>
				<p>Fax: (000) 000 00 00 0</p>
				<p>Email: <a href="Yaqoob:info@example.com"><span>info@mycompany.com</span></a></p>
			    <p>Follow on: <span>Facebook</span>, <span>Twitter</span></p>
			</div>
		</div>				
		<div class="col span_2_of_3">
			  <div class="contact-form">
			  	<h2>Contact Us</h2>
				    <form>
				    	<div>
					    	<span><label>NAME</label></span>
					    	<span><input type="text" value="" /></span>
					    </div>
					    <div>
					    	<span><label>E-MAIL</label></span>
					    	<span><input type="text" value="" /></span>
					    </div>
					    <div>
					     	<span><label>MOBILE</label></span>
					    	<span><input type="text" value="" /></span>
					    </div>
					    <div>
					    	<span><label>SUBJECT</label></span>
					    	<span><textarea> </textarea></span>
					    </div>
					   <div>
					   		<span><input type="submit" value="Submit"></span>
					  </div>
	     	    </form>
		    </div>
  		</div>				
	</div>						
</div>
<?php include_once('includes/footer.php'); ?>
</div>
</body>
</html>