
<!DOCTYPE HTML>
<html>
<head>
<title>The Free Extro-Electronics Website Template | Details :: w3layouts</title>
<?php include_once('includes/head.php'); ?>

</head>
<body>
<div class="wrap"> 
	<?php include_once('includes/header.php'); ?>
<?php include_once('includes/header-nav.php'); ?>

<div class="content">
	<div class="section group">
		<div class="cont span_2_of_3">
			<div class="single">
		      <h2><a href="index.html">Home</a></h2>
				<div class="grid-img1">
					<a href="images/pic4.jpg"><img src="images/pic4.jpg"></a> 
				</div>
				<div class="para">
					<h4>The Acon 35 is a 35 mm rangefinder camera made by Ars Optical in Japan in about 1956.</h4>
				<div class="cart-b">
					<button class="left rs">$329.58</button>
				    <div class="btn right"><a href="details.html">Add to Cart</a></div>
				    <div class="clear"></div>
				 </div>
				 <h5>100 items in stock</h5>
			   	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			   	<div class="btn top"><a href="details.html">More details</a></div>
			   	</div>
			   <div class="clear"></div>	
		   </div>
	   <div class="text-h1 top">
			<h2>20 other products in the same category</h2>
	  	</div>
	    <div class="div2 single-1">  
           <div class="sing-pic">		
        	 <img src="images/pic4.jpg" alt=""/>
		   </div>
		   <div class="sing-pic">
             <img src="images/pic5.jpg" alt="" />
		   </div>
		   <div class="sing-pic">
            <img src="images/pic6.jpg" alt="" />
		   </div>
		   <div class="sing-pic">
            <img src="images/pic7.jpg" alt=""/>
		   </div>		 		   
          <div class="clear"></div>		   
   		</div>
	</div>
	<div class="rsidebar span_1_of_3">
	  <div class="clear"></div>
		<div class="sidebar">
			<h2>Top Sellers</h2>
			<div class="listview_1_of_2 images_1_of_2">
				<div class="listimg listimg_2_of_1">
					<img src="images/pic4.jpg" alt=""/>
				</div>
				<div class="text list_2_of_1">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt  ut labore</p>
			  </div>
			</div>
	      <div class="listview_1_of_2 images_1_of_2">
				<div class="listimg listimg_2_of_1">
					  <img src="images/pic5.jpg" alt=""/>
				</div>
				<div class="text list_2_of_1">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt  ut labore</p>
		    </div>
	      </div>
			<div class="listview_1_of_2 images_1_of_2">
				<div class="listimg listimg_2_of_1">
					  <img src="images/pic6.jpg" alt=""/>
				</div>
				 <div class="text list_2_of_1">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt  ut labore</p>
				 </div>
		  </div>
		</div>
	  </div>
   </div>	
</div>
<?php include_once('includes/footer.php'); ?>
</div>
</body>
</html>